/*****************************************************
** Author: 
** Date: 2017-11-16
** Copyright(c) All Rights Reserved.
*****************************************************/

#ifndef __PRODUCER_H_
#define __PRODUCER_H_

#ifdef __cplusplus
extern "C"{
#endif

/***** INCLUDES *************************************/

/***** TYPEDEFS *************************************/

typedef struct SConsumer_t
{
	int ip;
	int port;
} SComsumer;

/***** DEFINES **************************************/

#define PRODUCER_NUM_MAX		10

#define PRODUCER_SHM_TAG		1234

/***** EXTERNAL VARIABLES ***************************/

/***** EXTERNAL FUNCTIONS ***************************/

/***** VARIABLES ************************************/

/***** PROTOTYPES ***********************************/

/***** CONSTANTS ************************************/


#ifdef __cplusplus
}
#endif

#endif /* __PRODUCER_H_ */

