/*****************************************************
** Author: 
** Date: 2017-11-15
** Copyright(c) All Rights Reserved.
*****************************************************/

/***** Include files ********************************/
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

/***** Define constant ******************************/

/***** Define structure *****************************/

/***** Define Prototype of functions ****************/

/***** Define global ********************************/

/**
 * @brief
 * @param  None
 * @retval None
 */
int main(int argc, char *argv[])
{
	pid_t pid;
	char *message;
	int n, ret;

	printf("fork program starting\n");
	pid = fork();
	switch(pid)
	{
		case -1: 
			printf("fork failed\n");
			exit(-1);
		case 0:
			message = "this is the child";
			n = 5;
			ret = 1;
			break;
		default:
			message = "this is the parent";
			n = 3;
			ret = 0;
			break;
	}

	for (; n> 0; n--)
	{
		puts(message);
		sleep(1);
	}

	if (pid != 0)
	{
		int child_ret;
		pid_t child_pid;

		child_pid = wait(&child_ret);
		printf("child has finished, pid: %d\n", child_ret);

		if (WIFEXITED(child_ret) != 0)
		{
			printf("child exited with code %d\n", WEXITSTATUS(child_ret));
		}
		else
		{
			printf("child terminated abnormally\n");
		}
	}

	exit(ret);
}

/* End of file */



